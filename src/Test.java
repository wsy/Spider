import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import com.gargoylesoftware.htmlunit.javascript.host.file.FileReader;

import Utils.DbUtil;
import Utils.DragUrls;
import Utils.DrapPaths;
import Utils.FileUtil;
import Utils.SetInterface;
import xinna.WeiboSet;

public class Test {
	public static final String URL = "http://blog.sina.com.cn/s/blog_15c2f7ed60102w44z.html";
	public static final String XINNA = "http://blog.sina.com.cn/";

	public static final void main(String[] args) {
		// FileUtil.getInstance().open();
		// DbUtil.getInstance().connect();
		SetInterface set = new WeiboSet();
		// DrapPaths xu = new DrapPaths(XINNA);
		// set.addPaths(xu.getUrls(set));
		// Set<String> paths = new HashSet<String>(set.pathPres());
		// for (String url : paths) {
		// xu = new DrapPaths(url);
		// set.addPaths(xu.getUrls(set));
		// System.out.println("paths count: " + set.pathPres().size());
		// }
		// write the path to the spider.txt
		// FileUtil.getInstance().open();
		// for (String path : set.pathPres()) {
		// FileUtil.getInstance().write(path + "\n");
		// }
		// FileUtil.getInstance().complete();

		// read path from the spider.txt
		DbUtil.getInstance().connect();
		String pre = set.pagePre();
		int count = 0;// count the page urls
		BufferedReader br = null;
		DragUrls du = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(
					"E:/spider.txt")));
			
			String path = null;
			while ((path = br.readLine()) != null) {
				// add the page urls to the db
				du = new DragUrls(path, pre);
				Set<String> urls = du.getUrls();
				System.out.println("urls count: " + (count += urls.size()));
				for (String page : urls) {
					// FileUtil.getInstance().write(page + "\n");
					DbUtil.getInstance().insert(page);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		DbUtil.getInstance().close();
		// FileUtil.getInstance().complete();
	}
}
