package Utils;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class DrapPaths {
	private String url;

	// 从这个url所指向的网页中，获取符合set限定的url
	public DrapPaths(String url) {
		this.url = url;
	}

	// return new added paths
	public Set<String> getUrls(final SetInterface set) {
		Set<String> newPaths = new HashSet<String>();
		HtmlPage page = null;
		try {
			page = LoadUtil.getInstance().getPage(url);
		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
			return newPaths;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return newPaths;
		}
		List<?> as = page.getByXPath("//a");
		HtmlAnchor anchor;
		String href;

		for (Object o : as) {
			anchor = (HtmlAnchor) o;
			href = anchor.getAttribute("href");
			for (String pre : set.pathPres()) {
				if (href.startsWith(pre) && !href.equals(pre)
						&& !href.startsWith(set.pagePre())) {
					newPaths.add(href);
				}
			}
		}
		return newPaths;
	}
}
