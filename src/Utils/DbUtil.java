package Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {
	private static DbUtil dbUtil;

	private DbUtil() {
	}

	public static DbUtil getInstance() {
		if (null == dbUtil)
			dbUtil = new DbUtil();
		return dbUtil;
	}

	// private PreparedStatement pst;

	public void connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(URL);
			// pst = conn.prepareStatement("insert into xinnaurl values (?)");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private int i = 0;

	public ResultSet get(int start, int length) {
		Statement stmt;
		String sql = null;
		try {
			stmt = conn.createStatement();
			sql = "select * from xinnaurl limit " + start + "," + length;
			return stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public ResultSet get() {
		Statement stmt;
		String sql = null;
		try {
			stmt = conn.createStatement();
			sql = "select * from xinnaurl";
			return stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void insert(String url) {
		Statement stmt;
		String sql = null;
		try {
			stmt = conn.createStatement();
			sql = "insert into xinnaurl values('" + url + "')";
			stmt.execute(sql);
		} catch (SQLException e) {
			System.out.println("re insert: " + url);
			// e.printStackTrace();
			// return false;
		}
	}

	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
	}

	private Connection conn = null;
	private static final String URL = "jdbc:mysql://localhost:3306/test?user=root&password=&useUnicode=true&characterEncoding=UTF8&jdbcCompliantTruncation=false";
}
