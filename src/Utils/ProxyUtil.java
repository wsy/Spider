package Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.gargoylesoftware.htmlunit.ProxyConfig;

public class ProxyUtil {
	private static ProxyUtil proxyUtil;
	private List<ProxyConfig> proxyes;

	private ProxyUtil() {
		proxyes = new ArrayList<ProxyConfig>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("proxy.txt"));
			// BufferedReader br = new BufferedReader(new FileReader(
			// "E:/proxy.txt"));
			String data;
			while ((data = br.readLine()) != null) {
				String[] args = data.split(":");
				proxyes.add(new ProxyConfig(args[0].trim(), Integer
						.valueOf(args[1])));
			}
		} catch (IOException e) {
			// e.printStackTrace();
		}
		proxyes.add(new ProxyConfig("127.0.0.1", 80));
	}

	public static ProxyUtil getInstance() {
		if (null == proxyUtil) {
			proxyUtil = new ProxyUtil();
		}
		return proxyUtil;
	}

	synchronized public ProxyConfig getProxy() {
		if (proxyes.size() == 0) {
			return null;
		}
		ProxyConfig pc = proxyes.get(0);
		proxyes.remove(0);
		return pc;
	}

	synchronized public void addProxy(ProxyConfig p) {
		proxyes.add(p);
	}
}
