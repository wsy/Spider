package Utils;

import java.util.Set;

public interface SetInterface {

	public String pagePre();

	public Set<String> pathPres();

	public void addPath(String path);

	public void addPaths(Set<String> newPaths);
}
