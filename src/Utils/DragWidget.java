package Utils;

import java.io.IOException;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class DragWidget {
	private HtmlPage page;

	public DragWidget(String url) {
		try {
			page = LoadUtil.getInstance().getPage(url);
		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cope(String... paths) {
		getAnchor(paths[0]);
		getInput(paths[1]);
		getImg(paths[2]);
	}

	// href content target id title onclick
	private void getAnchor(String anchorPath) {
		FileUtil.getInstance().open(anchorPath);
		// FileUtil.getInstance().write(
		// "href:\t content:\t target:\t id:\t title:\t onclick:\t \n");
		List<?> as = page.getByXPath("//a");
		HtmlAnchor a;
		String href, content, target, id, title, onclick;
		for (Object o : as) {
			a = (HtmlAnchor) o;
			href = "href: " + a.getAttribute("href");
			content = "content: " + a.asText();
			target = "target: " + a.getAttribute("target");
			id = "id: " + a.getAttribute("id");
			title = "title: " + a.getAttribute("title");
			onclick = "onclick: " + a.getAttribute("onclick");
			FileUtil.getInstance().write(
					href + ";\t" + content + ";\t" + target + ";\t" + id
							+ ";\t" + title + ";\t" + onclick + "\n");
		}
		FileUtil.getInstance().complete();
	}

	// id classes type name autocomplete onclick value disabled
	private void getInput(String inputPath) {
		FileUtil.getInstance().open(inputPath);
		// FileUtil.getInstance()
		// .write("id:\t classes:\t type:\t name:\t autocomplete:\t onclick:\t value:\t disabled:\n");
		List<?> inputs = page.getByXPath("//input");
		HtmlInput input = null;
		String id, classes, type, name, autocomplete, onclick, value, disabled;
		for (Object o : inputs) {
			input = (HtmlInput) o;

			id = "id: " + input.getAttribute("id");
			classes = "classes: " + input.getAttribute("class");
			type = "type: " + input.getAttribute("type");
			name = "name: " + input.getAttribute("name");
			autocomplete = "autocomplete: "
					+ input.getAttribute("autocomplete");
			onclick = "onclick: " + input.getAttribute("onclick");
			value = "value: " + input.getAttribute("value");
			disabled = "disabled: " + input.getAttribute("disabled");
			FileUtil.getInstance().write(
					id + ";\t" + classes + ";\t" + type + ";\t" + name + ";\t"
							+ autocomplete + ";\t" + onclick + ";\t" + value
							+ ";\t" + disabled + "\n");
		}
		FileUtil.getInstance().complete();
	}

	// src alt
	private void getImg(String imgPath) {
		FileUtil.getInstance().open(imgPath);
		FileUtil.getInstance().write("src:\t alt:\t\n");
		List<?> imgs = page.getByXPath("//img");
		HtmlImage img;
		String src, alt;
		for (Object o : imgs) {
			img = (HtmlImage) o;
			src = "" + img.getAttribute("src");
			alt = "" + img.getAttribute("alt");
			FileUtil.getInstance().write(src + "\t" + alt + "\n");
		}
		FileUtil.getInstance().complete();
	}
}
