package Utils;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class DragUrls {
	private String url;
	private String pre;

	// 对于输入的url,爬取它的页面，搜索当中符合pre前缀的所有url
	public DragUrls(String url, String pre) {
		this.url = url;
		this.pre = pre;
	}

	public Set<String> getUrls() {
		Set<String> urls = new HashSet<String>();
		HtmlPage page = null;
		try {
			page = LoadUtil.getInstance().getPage(url);
		} catch (FailingHttpStatusCodeException e) {
			// e.printStackTrace();
			return urls;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return urls;
		}
		// com.gargoylesoftware.htmlunit.xml.XmlPage page2=null;
		List<?> as = page.getByXPath("//a");
		HtmlAnchor a = null;
		String href;
		for (Object o : as) {
			a = (HtmlAnchor) o;
			href = a.getAttribute("href");
			if (href.startsWith(pre)) {
				if (href.contains("?")) {
					href = href.split("\\?")[0];
				}
				if (href.contains("#")) {
					href = href.split("#")[0];
				}
				urls.add(href);
			}
		}
		return urls;
	}
}
