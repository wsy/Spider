package Utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtil {
	private static FileUtil fileUtil;
	private FileWriter fw;
	private static final String FILEPATH = "E:/spider.txt";

	private FileUtil() {

	}

	public void open(String filePath) {
		try {
			fw = new FileWriter(new File(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void open() {
		try {
			fw = new FileWriter(new File(FILEPATH));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(String content) {
		try {
			fw.write(content);
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println(content);
		}
	}

	public void complete() {
		try {
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static FileUtil getInstance() {
		if (null == fileUtil) {
			fileUtil = new FileUtil();
		}
		return fileUtil;
	}

}
