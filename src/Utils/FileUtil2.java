package Utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtil2 {
	private FileWriter fw;

	public FileUtil2(String path) {
		try {
			File file=new File(path);
			if(!file.exists()){
				file.createNewFile();
			}
			fw = new FileWriter(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(String line) {
		try {
			fw.write(line + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void complete() {
		try {
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
