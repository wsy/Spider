package Utils;

import java.io.IOException;
import java.net.MalformedURLException;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class LoadUtil {

	private static LoadUtil loadUtil;
	private final WebClient webClient;

	private LoadUtil() {
		webClient = new WebClient(BrowserVersion.FIREFOX_38);
		webClient.getOptions().setCssEnabled(false);
		// final DefaultCredentialsProvider credentialsProvider =
		// (DefaultCredentialsProvider) webClient
		// .getCredentialsProvider();
		// credentialsProvider.addCredentials("username", "password");
	}

	public static LoadUtil getInstance() {
		if (null == loadUtil) {
			loadUtil = new LoadUtil();
		}
		return loadUtil;
	}

	public HtmlPage getPage(String url) throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		webClient.getOptions().setJavaScriptEnabled(false);
		webClient.getOptions().setTimeout(5000);
		ProxyConfig pc = ProxyUtil.getInstance().getProxy();
		if (pc != null)
			webClient.getOptions().setProxyConfig(pc);
		System.out.print("proxy ip: " + pc.getProxyHost() + " ; port: "
				+ pc.getProxyPort() + " ");
		Object object = null;
		// 注意这里不能catch,在ReadFiles里面要捕捉时间来调整线程水米按时间
		if (pc != null)
			// 注意这里其实代理就自适应了，因为如果没法获取page，则不会执行到这句，也就是放弃了该代理
			object = webClient.getPage(url);
		ProxyUtil.getInstance().addProxy(pc);
		pc = null;
		if (object != null && object instanceof HtmlPage) {
			return (HtmlPage) object;
		} else {
			return null;
		}
	}

	public HtmlPage getAjaxPage(String url) {
		enableAjax();
		HtmlPage dynamicPage = null;
		try {
			dynamicPage = (HtmlPage) webClient.getPage(url);
		} catch (FailingHttpStatusCodeException e) {
			// e.printStackTrace();
		} catch (MalformedURLException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		}
		webClient.waitForBackgroundJavaScript(1000 * 3);
		webClient.setJavaScriptTimeout(0);
		return dynamicPage;
	}

	private void enableAjax() {
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setActiveXNative(false);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.waitForBackgroundJavaScript(10000);// 设置JS后台等待执行时间
		webClient.setAjaxController(new NicelyResynchronizingAjaxController());// 很重要，设置支持AJAX
		webClient.getOptions().setJavaScriptEnabled(true);
	}
}
