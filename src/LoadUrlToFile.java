import java.sql.ResultSet;
import java.sql.SQLException;

import Utils.DbUtil;
import Utils.FileUtil2;

public class LoadUrlToFile {
	private static final int GAP=50000;
	public static void main(String[] args) {
		DbUtil.getInstance().connect();
		ResultSet rs1=DbUtil.getInstance().get();
		FileUtil2 fu=new FileUtil2("E:/1.txt");
		try {
			while(rs1.next()){
				fu.write(rs1.getString("url"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
