package xinna;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlHeading1;
import com.gargoylesoftware.htmlunit.html.HtmlHeading2;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import com.gargoylesoftware.htmlunit.html.HtmlStrong;

import Utils.LoadUtil;

public class WeiboBlog {
	private String url;

	public WeiboBlog(String url) {
		this.url = url;
		content = "";
		title = "";
		time = "";
		author = "";
	}

	private String content;
	private String title;
	private String time;
	private String author;

	public String getContent() {
		return content;
	}

	public String getTitle() {
		return title;
	}

	public String getTime() {
		return time;
	}

	public String getAuthor() {
		return author;
	}

	public String getUrl() {
		return url;
	}

	//
	private static final String[] CONTENT = {
			"//div[@class='articalContent   ']",
			"//div[@class='articalContent newfont_family']",
			"//div[@class='articalContent   newfont_family']",
			"//div[@id='sina_keyword_ad_area2']", "//div[@class='BNE_cont']" };
	// articalContent newfont_family

	//
	private static final String[] TITLE = { "//h2[@class='titName SG_txta']",
			"//h1[@class='h1_tit']" };

	//
	private static final String[] TIME = { "//span[@class='time SG_txtc']",
			"//span[@class='time SG_txtc']" };

	//
	private static final String[] AUTHOR = { "//strong[@id='ownernick']",
			"//span[@id='blognamespan']" };

	private List<?> get(String[] names) {
		List<?> ls;
		for (String name : names) {
			ls = page.getByXPath(name);
			if (ls != null && ls.size() > 0) {
				return ls;
			}
		}
		return null;
	}

	private HtmlPage page;

	public boolean getInfo() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		page = LoadUtil.getInstance().getPage(url);
		if (null == page) {
			// url += " page is null";
			return false;
		}

		// get title
		List<?> h2s = get(TITLE);
		if (h2s == null) {
			// url += " title";
			return false;
		}
		Object h2 = h2s.get(0);
		if (h2 instanceof HtmlHeading2) {
			title = ((HtmlHeading2) h2).asText();
		} else {
			title = ((HtmlHeading1) h2).asText();
		}

		// get time
		List<?> spans = get(TIME);
		if (spans == null) {
			// url += " time";
			return false;
		}
		HtmlSpan span = (HtmlSpan) spans.get(0);
		time = span.asText();

		// get content
		List<?> divs = get(CONTENT);
		if (divs == null) {
			// url += " content";
			return false;
		}
		HtmlDivision div = (HtmlDivision) divs.get(0);
		content = div.asText();

		// get author
		List<?> strongs = get(AUTHOR);
		if (strongs == null) {
			// url += " author";
			return false;
		}
		Object strong = strongs.get(0);
		if (strong instanceof HtmlStrong) {
			author = ((HtmlStrong) strong).asText();
		} else {
			author = ((HtmlSpan) strong).asText();
		}
		return true;
	}
}
