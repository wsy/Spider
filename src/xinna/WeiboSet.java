package xinna;

import java.util.HashSet;
import java.util.Set;

import Utils.SetInterface;

public class WeiboSet implements SetInterface {
	public WeiboSet() {
		paths = new HashSet<String>();
		paths.add("http://blog.sina.com.cn");
		paths.add("http://blog.sina.com.cn/lm");
		paths.add("http://blog.sina.com.cn/list");
	}

	private Set<String> paths;

	private static final String PRE_BLOG = "http://blog.sina.com.cn/s/blog_";

	@Override
	public String pagePre() {
		// TODO Auto-generated method stub
		return PRE_BLOG;
	}

	@Override
	public Set<String> pathPres() {
		return paths;
	}

	@Override
	public void addPath(String path) {
		// System.out.println("add path: " + path);
		// TODO Auto-generated method stub
		if (null == paths)
			paths = new HashSet<String>();
		if (!paths.contains(path)) {
			paths.add(path);
		}
	}

	@Override
	public void addPaths(Set<String> newPaths) {
		// TODO Auto-generated method stub
		paths.addAll(newPaths);
	}
}
