import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

import xinna.WeiboBlog;

public class ReadFiles {

	private static final int CURR = 1;

	public static final void main(String[] args) throws IOException,
			InterruptedException {

		FileWriter error = new FileWriter(new File("error.txt"));
		// windows
		// FileWriter error = new FileWriter(new File("E:/error.txt"));

		File urlFile = new File("urls/url" + CURR + ".txt");
		// windows
		// File urlFile = new File("E:/urls.txt");

		BufferedReader br = new BufferedReader(new FileReader(urlFile));
		int total = 0;
		int success = 0;
		// int sleep = 0;
		String url = br.readLine();
		do {
			System.out.print("total " + total++ + ": ");
			WeiboBlog wb = new WeiboBlog(url);
			boolean get = false;
			try {
				get = wb.getInfo();
			} catch (FailingHttpStatusCodeException | IOException e) {
				// expect to catch connect time out or refused
				error.write(url + "\n");
				error.flush();
				System.out.println(url + " net");
				continue;
			}
			if (get) {
				File file = new File("pages" + CURR + "/" + (success++)
						+ ".txt");
				// windows
				// File file = new File("E:/pages/" + (success++) + ".txt");
				file.createNewFile();
				FileWriter fw = new FileWriter(file);
				fw.write(wb.getTitle() + "\n" + wb.getAuthor() + "\n"
						+ wb.getTime() + "\n" + wb.getContent());
				fw.flush();
				fw.close();
				System.out.println("success: " + success);
			} else {
				error.write(wb.getUrl() + "\n");
				error.flush();
				System.out.println(wb.getUrl() + " content");
			}
			url = br.readLine();
		} while (url != null);

		error.flush();
		error.close();
	}
}