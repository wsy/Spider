import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import Utils.FileUtil2;

public class SplitUrls {
	private static int TOTAL = 15814;

	public static void main(String[] args) throws IOException {
		File urlFile = new File("E:/urls.txt");
		BufferedReader br = new BufferedReader(new FileReader(urlFile));
		for (int i = 0; i < TOTAL; i++) {
			br.readLine();
		}
		int j = 0;
		int fileCount = 1;
		FileUtil2 fu = new FileUtil2("E:/urls/url" + fileCount + ".txt");
		String data;
		while ((data = br.readLine()) != null) {
			fu.write(data);
			j++;
			if (j == 10000) {
				fu.complete();
				fileCount++;
				fu = new FileUtil2("E:/urls/url" + fileCount + ".txt");
				j = 0;
			}
		}
		fu.complete();
	}
}
